package com.codefiship.qr1922

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.commit
import androidx.fragment.app.replace

class MainActivity : AppCompatActivity() {

    companion object {
        const val PERMISSIONS_REQUEST_CODE = 1;
        val PERMISSIONS = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.SEND_SMS
        );
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                replace<PermissionFragment>(R.id.fragment_container_view)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        checkPermission()
    }

    private fun checkPermission() {
        PERMISSIONS.all {
            ContextCompat.checkSelfPermission(
                this,
                it
            ) == PackageManager.PERMISSION_GRANTED
        }.let {
            if (it) {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<ScannerFragment>(R.id.fragment_container_view)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<ScannerFragment>(R.id.fragment_container_view)
                }
            } else {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<PermissionFragment>(R.id.fragment_container_view)
                }
                Toast.makeText(this@MainActivity, R.string.permission_denied, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

}