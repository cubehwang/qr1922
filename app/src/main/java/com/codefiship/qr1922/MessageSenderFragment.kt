package com.codefiship.qr1922

import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.*
import android.os.Bundle
import android.telephony.SmsManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class MessageSenderFragment : BottomSheetDialogFragment() {

    companion object {
        const val TAG = "MessageSenderFragment"
        const val ARG_TEXT = "TEXT"
        const val MESSAGE_SENT_INTENT = "com.codefiship.qr1922.MESSAGE_SENT_INTENT"
        const val STATUS_UNSET = "UNSET"
        const val STATUS_SENDING = "SENDING"
        const val STATUS_SUCCEED = "SUCCEED"
        const val STATUS_FAILED = "FAILED"

        fun newInstance(text: String) = MessageSenderFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_TEXT, text)
            }
        }
    }

    private lateinit var message: String
    private lateinit var sendingLayout: View
    private lateinit var sentSucceedLayout: View
    private lateinit var sentFailedLayout: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_message_sender, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sendingLayout = view.findViewById<View>(R.id.sendingLayout)
        sentSucceedLayout = view.findViewById<View>(R.id.sentSucceedLayout)
        sentFailedLayout = view.findViewById<View>(R.id.sentFailedLayout)
        arguments?.getString(ARG_TEXT)?.let {
            view.findViewById<TextView>(R.id.textViewMessage).text = it
            message = it
            if (it.isEmpty()) {
                dismiss()
            }
        }
        setStatus(STATUS_UNSET)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        (requireParentFragment() as DialogInterface.OnDismissListener).onDismiss(dialog)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().registerReceiver(broadcastReceiver, IntentFilter(MESSAGE_SENT_INTENT))
        sendSMS(message)
    }

    override fun onPause() {
        super.onPause()
        requireActivity().unregisterReceiver(broadcastReceiver)
        dismiss()
    }

    @SuppressLint("UnlocalizedSms")
    private fun sendSMS(text: String) {
        if (text.isNotEmpty()) {
            setStatus(STATUS_SENDING)
            val sentPendingIntent = PendingIntent.getBroadcast(
                requireContext(),
                0,
                Intent(MESSAGE_SENT_INTENT),
                0
            )
            SmsManager.getDefault().apply {
                sendTextMessage(
                    "1922",
                    null,
                    "${text}_",
                    sentPendingIntent,
                    null,
                )
            }
        }
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (resultCode == Activity.RESULT_OK) {
                setStatus(STATUS_SUCCEED)
            } else {
                setStatus(STATUS_FAILED)
            }
        }
    }

    private fun setStatus(status: String) {
        sendingLayout.visibility = if (status == STATUS_SENDING) View.VISIBLE else View.GONE
        sentSucceedLayout.visibility = if (status == STATUS_SUCCEED) View.VISIBLE else View.GONE
        sentFailedLayout.visibility = if (status == STATUS_FAILED) View.VISIBLE else View.GONE
    }
}