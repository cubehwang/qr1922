package com.codefiship.qr1922

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment

class PermissionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_permission, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<View>(R.id.requestPermissionButton).setOnClickListener {
            ActivityCompat.requestPermissions(
                requireActivity(),
                MainActivity.PERMISSIONS,
                MainActivity.PERMISSIONS_REQUEST_CODE
            )
        }
    }

}