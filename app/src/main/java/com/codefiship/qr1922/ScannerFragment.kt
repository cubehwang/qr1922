package com.codefiship.qr1922

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ScanMode
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result

class ScannerFragment : Fragment(), DialogInterface.OnDismissListener {

    private lateinit var codeScanner: CodeScanner

    private val messageRegex = Regex("SMSTO:1922:(.+)", setOf(RegexOption.DOT_MATCHES_ALL, RegexOption.IGNORE_CASE))

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()
        codeScanner = CodeScanner(activity, scannerView).apply {
            formats = listOf(BarcodeFormat.QR_CODE)
            scanMode = ScanMode.CONTINUOUS
            decodeCallback = DecodeCallback {
                activity.runOnUiThread { onScanned(it) }
            }
        }
        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    private fun onScanned(result: Result) {
        messageRegex.find(result.text)?.let {
            it.groupValues[1]
        }?.let {
            Log.d("SCAN", it)
            codeScanner.stopPreview()
            MessageSenderFragment.newInstance(it)
                .show(childFragmentManager, MessageSenderFragment.TAG)
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        codeScanner.startPreview()
    }
}